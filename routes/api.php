<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->post('/auth', ['as' => 'auth', 'uses' => 'ApiAuthController@auth']);

Route::apiResource('/message', 'ApiMessageController')->only(['store']);
