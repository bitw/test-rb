<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function token()
    {
        return $this->hasMany(Token::class);
    }

    public function scopeGroup($query, $group)
    {
        if (is_array($group)) {
            $query->whereIn('group', $group);
        } else {
            $query->where('group', $group);
        }
    }
}
