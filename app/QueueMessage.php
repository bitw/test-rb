<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QueueMessage extends Model
{
    protected $table = 'queue_message';

    protected $fillable = [
        'message',
        'token',
        'type',
    ];

    public function scopeNotSended($query){
        return $query
            ->where('attempt', '<', 3)
            ->where('sended', false);
    }
}
