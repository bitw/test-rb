<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'tokens';

    protected $fillable = [
        'token',
        'type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeLogin($query, $token, $type)
    {
        return $query
            ->where('token', $token)
            ->where('type', $type);
    }
}
