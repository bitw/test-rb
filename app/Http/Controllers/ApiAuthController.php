<?php

namespace App\Http\Controllers;

use App\Token;
use App\User;
use Faker\Factory;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class ApiAuthController extends Controller
{
    /**
     * Аутентификация пользователя по ключу и создание ключа для устройства
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function auth(Request $request)
    {
        if(!$request->filled('type')){
            return response()->json([
                'message' => 'Type not present'
            ], 400);
        }

        $user = auth()->user();

        $token = $request->token;

        // Если ключ устройства не указан
        if(!$request->filled('token')){
            // Генерируем уникальный ключ
            $token = str_random(32);
        }

        // прикрепляем ключ устройства к учетке пользователя
        $user->token()->create([
            'token' => $token,
            'type' => $request->type
        ]);

        // Отдаем ответ с сгенерированным или указанным ключем
        return response()->json([
            'data' => ['token'=>$token]
        ], 200);
    }
}
