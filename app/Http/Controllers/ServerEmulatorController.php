<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\Resource;

class ServerEmulatorController extends Controller
{
    /**
     * Эмуляция отправки сообщения
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // Коды результатов
        $results = [
            200 => 'Message sended',
            403 => 'Token is blocked',
            404 => 'Token or transport not found',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
        ];

        // Выбираем случайный результат
        $result = array_rand($results);

        // Отдаем случайный результат клиенту в JSON формате
        return response()->json(
            [
                'code' => $result,
                'message' => $results[$result]
            ],
            $result
        );
    }
}
