<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiMessageController extends Controller
{
    /**
     * Создание сообщения
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        // создаем объкт сообщения
        $message = \Message::text($request->text);

        // Если указан пользователь(и)
        if($request->filled('user')){
            // передаем пользователя(ей) в объект сообщения
            $message->user($request->user);
        }

        // если указана группа
        if($request->filled('group')){
            // передаем ее в объект сообщения
            $message->group($request->group);
        }
dd($message);
        try{
            // сохраняем сообщение
            $message->store();
        }catch(\Exception $exception){
            // если в процессе сохранения произошла ошибка то отдаем сообщение об ошибке
            return response()->json(['code'=>400, 'message'=>$exception->getMessage()], 400);
        }

        // выводим сообщение об успешном создании
        return response()->json(['code'=>200, 'message'=>'Messages are queued']);
    }
}
