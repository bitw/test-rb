<?php

namespace App\Console\Commands;

use App\QueueMessage as QueueMessageModel;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Console\Command;
use GuzzleHttp\Client;

/**
 * Отправка очереди сообщений
 *
 * Class QueueMessage
 * @package App\Console\Commands
 */
class QueueMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queue-message:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Получаем неотправленные сообщения
        QueueMessageModel::NotSended()
            // по 25 элементов
            ->chunk(25, function ($messages) {
                $client = new Client();
                foreach ($messages as $message) {
                    $this->info('Sending by token "' . $message->token . '" as ' . $message->type . ": " . $message->message . '');

                    // Пробуем выполнить отправку
                    try {
                        $result = $client->get(
                            'http://127.0.0.1:8000/emulator',
                            ['query' => $message->only('message', 'token', 'type')]
                        );

                        // выводим статус об успешной отправке
                        $this->line($result->getStatusCode() . ': ' . $result->getReasonPhrase());

                        // Выставляем признак успешной отправки
                        $message->sended = true;
                    } catch (ClientException | ServerException $exception) {
                        // Поймали ошибку
                        // выводим сообщения об сбое при отправке
                        $this->error($exception->getCode() . ': ' . $exception->getResponse()->getReasonPhrase());

                        // Икреминируем счетчик ошибок для сообщения
                        $message->attempt++;
                    }

                    // Сохраняем состояние сообщения
                    $message->save();
                }
            });
    }
}
