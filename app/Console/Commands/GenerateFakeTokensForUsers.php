<?php

namespace App\Console\Commands;

use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;

/**
 * Генерация фейковых ключей и типов для устройств пользователя
 *
 * Class GenerateFakeTokensForUsers
 * @package App\Console\Commands
 */
class GenerateFakeTokensForUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-fake-tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $types = ['email', 'sms', 'push'];

        $client = new Client();
        for ($i = 0; $i <= 200; $i++) {
            $user = User::inRandomOrder()->first();

            try {
                $response = $client->post(
                    'http://127.0.0.1:8000/api/auth',
                    [
                        'query' => [
                            'api_token' => $user->api_token,
                            'type' => array_random($types)
                        ]
                    ]
                );
                $this->info('Make random token for ' . $user->name . ' ... ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase());
            } catch (ClientException $exception) {
                $this->error($exception->getResponse()->getStatusCode() . ' ' . $exception->getResponse()->getReasonPhrase());
                break;
            }
        }
    }
}
