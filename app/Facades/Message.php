<?php

namespace App\Facades;

use App\QueueMessage;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class Message
{
    protected $text = null;
    protected $token = null;

    public function __construct($text = null)
    {
        $this->text = $text;
    }

    public function text($text)
    {
        $this->text = $text;
        return $this;
    }

    public function group($group)
    {
        $users = User::group($group)->get();
        foreach ($users as $user) {
            $this->user($user);
        }
    }

    public function user($user)
    {
        // Если это экземпляр модели User
        if ($user instanceof User) {
            // Запоминаем все пары токенов и типов
            $user->token()->each(function ($item) {
                $this->token($item->token, $item->type);
            });
        }

        // Если это массив значений
        if (is_array($user)) {
            foreach ($user as $usr) {
                $this->user($usr);
            }
        }

        // Если пердан идентификатор пользователя
        if (is_numeric($user)) {
            // Получаем модель пользователя
            $user = User::find($user);
            // перезапускаем метод для запоминания токенов и типов
            $this->user($user);
        }

        // Если это коллекция моделей User
        if ($user instanceof Collection) {
            // Перебираем коллекция
            $user->each(function ($user) {
                // Перезапускаем метод для получения токенов и типов
                $this->user($user);
            });
        }

        return $this;
    }

    public function token($token, $type)
    {
        if (empty($this->token)) {
            $this->token = collect();
        }

        $this->token->push([
            'token' => $token,
            'type' => $type
        ]);

        return $this;
    }

    public function store()
    {
        if (empty($this->text)) {
            throw new \Exception("Message is empty");
        }

        if(!count($this->token)){
            throw new \Exception("Token(s) not assigned");
        }

        // Уникализируем коллекцию токенов
        $this->token = $this->token->unique();

        // Добавляем к каждому токену сообщение
        $this->token->transform(function ($item) {
            $item['message'] = $this->text;
            return $item;
        });

        // Создаем записи о сообщении в таблицу
        $this->token->each(function ($item) {
            QueueMessage::create($item);
        });

        // Обнуляем текст и коллекцию ключей на случай если необходимо создать еще коллекцию ключей и сообщений
        $this->text = $this->token = null;

        return $this;
    }
}