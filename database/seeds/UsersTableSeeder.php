<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    // Создоваемое количество учетных записей
    protected $count = 100;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');

        $groups = ['admin', 'manager', 'user'];
        for ($i = 1; $i <= $this->count; $i++) {
            \App\User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => bcrypt(str_random(6)),
                'api_token' => str_random(32),
                'group' => array_random($groups)
            ]);
        }
    }
}
